package views;

import utils.Tools;
import utils.ToolsMapper;

import javax.swing.*;
import javax.xml.transform.Source;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**@author Chen175
 *
 */
public class RaspberryPiWindow  extends JFrame {
    //    静态常量
    final static  String DIR = System.getProperty("user.dir");
    final static  Integer FONTSIZE = 20;
    final static  Font FONT = new Font("楷体",Font.BOLD,FONTSIZE);
    final static  Tools tools = new ToolsMapper();
//    target/classes/
    final  String Sourcepath = getClass().getResource("/").getPath().replaceFirst("/","");

    //    屏幕尺寸
    Dimension scrSize=Toolkit.getDefaultToolkit().getScreenSize();
    final int WIDTH = scrSize.width;
    final int HEIGHT = scrSize.height;
    // 创建选项卡面板
    final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
    //组件
//    面板
    JPanel pane = new JPanel();
    JPanel ImageView = new JPanel();
    JLabel ImageView_show = new JLabel();
    ImageIcon image = new ImageIcon(Sourcepath+"img/test/1.jpg");

    public RaspberryPiWindow() throws HeadlessException {
        this.setTitle("树莓派客户端");
        SetImageView();


//  ImageView  显示图片


//        选项卡面板  tabbedPane
        tools.setFont(tabbedPane,20);
        ChangeTabColor(tabbedPane,Color.red,Color.GRAY);
        tabbedPane.setForeground(Color.RED);
        tabbedPane.setBackground(Color.GRAY);
        tabbedPane.add(ImageView,"显示图片面板");
        /**
         * true无边框 全屏显示
         * false有边框 全屏显示
         */
//        this.setUndecorated(true);
        this.setSize(scrSize);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.add(tabbedPane);
        this.setVisible(true);

    }

//   设置  ImageView  面板
    private  void SetImageView(){
        ImageView.setLayout(new BorderLayout());
        ImageView_show = new JLabel(image);
        ImageView_show.setPreferredSize(scrSize);
        ImageView.add(ImageView_show,BorderLayout.CENTER);
    }

    //    设置# TabbedPane选卡颜色
    public void ChangeTabColor(Component tab,Color foreground,Color background){
        tab.setBackground(background);
        tab.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int index = tabbedPane.getSelectedIndex();
                for (int i = 0; i < tabbedPane.getTabCount(); i++) {
                    if (i==index){
                        tabbedPane.setForegroundAt(i, foreground);
                    }else
                        tabbedPane.setForegroundAt(i, background);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

    }


}
