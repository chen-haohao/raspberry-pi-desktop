package utils;

import java.awt.*;

public class ToolsMapper implements Tools{

    @Override
    public Dimension getSreenSize() {
        Dimension scrSize=Toolkit.getDefaultToolkit().getScreenSize();
        return scrSize;
    }

    @Override
    public void setFont(Component com, int size) {
        com.setFont(new Font("楷体",Font.BOLD,size));
    }

//    设置组件居中
    @Override
    public void setComponentCenter(Component com) {
        Toolkit kit = Toolkit.getDefaultToolkit(); // 定义工具包
        Dimension screenSize = kit.getScreenSize(); // 获取屏幕的尺寸
        int screenWidth = screenSize.width/2; // 获取屏幕的宽
        int screenHeight = screenSize.height/2; // 获取屏幕的高
        int height = com.getHeight();
        int width = com.getWidth();
        com.setLocation(screenWidth-width/2, screenHeight-height/2);
    }
//设置多个组件的字体
    @Override
    public void setFont(Font font, Component... components) {
        for (Component component : components) {
            component.setFont(font);
        }
    }
//设置多个组件的PreferredSize （首选尺寸）
    @Override
    public void setComponetsPreferredSize(Dimension dimension, Component... components) {
        for (Component component : components) {
            component.setPreferredSize(dimension);
        }

    }


}
