import com.alee.laf.WebLookAndFeel;
import com.alee.skin.modena.ModenaSkin;
import views.RaspberryPiWindow;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        SwingUtilities.invokeLater (new Runnable ()
        {
            @Override
            public void run ()
            {
                // Install WebLaF as application LaF
                WebLookAndFeel.install (ModenaSkin.class);
                try {
                    UIManager.setLookAndFeel ( "com.alee.laf.WebLookAndFeel" );
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (UnsupportedLookAndFeelException e) {
                    e.printStackTrace();
                }
                // UIManager.setLookAndFeel ( WebLookAndFeel.class.getCanonicalName () );

                // You can also configure other WebLaF managers as you like now
                // StyleManager
                // SettingsManager
                // LanguageManager
                // ...

                // Initialize your application once you're done setting everything up
                // JFrame frame = ...

                // You can also use Web* components to get access to some extended WebLaF features
                // WebFrame frame = ...
                try {
                    RaspberryPiWindow window = new RaspberryPiWindow();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } );

    }
}
